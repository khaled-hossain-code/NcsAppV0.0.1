/**
 * Created by khaled on 5/28/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var timestamps = require('mongoose-timestamp');
var RoomTypes = 'Single VIP DoubleShare MaleWard FemaleWard ICU CCU MICU Dialysis '.split(' ');

var deviceSchema= new Schema({
  IP:{
    type: String,
    unique: true,
    required: true,
    index: true,
    match: /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
  },
  Floor:{
    type:String,
    required:true
  },
  RoomType:{
    type:String,
    required:true,
    enum:RoomTypes
  },
  RoomNumber:{
    type:String,
    required:true
  },
  BedNumber:{
    type:String,
    required:true
  },
  createdOn:{
    type:String
  },
  updatedOn:{
    type:String
    }
});

deviceSchema.plugin(timestamps);
//Export model
module.exports = mongoose.model('device',deviceSchema);