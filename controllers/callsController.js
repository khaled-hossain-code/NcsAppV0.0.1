/**
 * Created by khaled on 5/29/2016.
 */
var CallModel = require('../models/callsModel');
var _ = require('underscore');
var date = require('../Utilities/date.js');
var Device = require('../models/deviceModel');

exports.createcalls = function(req, res) {
  req.body = _.pick(req.body, 'IP', 'CallType', 'CallDate', 'StartTime', 'StopTime', 'DiffTime');

  Device.find({IP: req.body.IP}, function (err, device) {
    if (err) {
      res.status(500).send(err);
    } else if (_.isEmpty(device)) {
      res.status(404).end();
    } else {
      var call = new CallModel(req.body);
      call.createdOn = date().formatted;

      call.save(function (err) {
        if (err) {
          res.status(500).send(err); // 500 means server internal error
        } else {
          res.status(201).send(call); // status 201 means created
        }
      });
    }
  });
};

exports.getAllcalls = function (req, res){
  var query = _.pick(req.query, 'IP', 'CallType', 'CallDate', 'StartTime', 'StopTime', 'DiffTime');

  query = _.mapObject( query, function(val, key) {
      try{
        val = JSON.parse(val); //if the value is not JSON it returns error
      }catch(e){}

      return val;
  });

  CallModel.find(query, function(err, calls){
    if(err){
      res.status(500).send(err);
    }else if(_.isEmpty(calls)) {
      res.status(404).send("Not Found");
    }
    else {
      var callsWithLink = [];

      calls.forEach(function (call, index, array) {
        var newCall = call.toJSON();
        newCall.links = {};
        newCall.links.self = 'http://' + req.headers.host + '/api/calls/' + newCall._id;
        callsWithLink.push(newCall);
      });
      res.send(callsWithLink);
    }
  });
};

exports.findCallByID = function(req, res, next){

  CallModel.findById(req.params.callsID,function(err, call){
    if(err)
    {
      res.status(500).send(err);
    }else if(_.isEmpty(call)){
      res.status(404).send("Not Found");
    }else{
      req.call = call;
      next();
    }
  });
};


exports.getCall =  function(req, res){
  var callWithLinks = req.call.toJSON();
  callWithLinks.links = {};

  callWithLinks.links.finterByCallType = 'http://'+ req.headers.host + '/api/calls/?CallType=' + callWithLinks.CallType;
  callWithLinks.links.finterByIP = 'http://'+ req.headers.host + '/api/calls/?IP=' + callWithLinks.IP;

  callWithLinks.links.finterByCallDate = 'http://'+ req.headers.host + '/api/calls/?CallDate=' + callWithLinks.CallDate;
  callWithLinks.links.finterByCallDateLessThan = 'http://'+ req.headers.host + '/api/calls/?CallDate=' + '{%22$lt%22:%22' + callWithLinks.CallDate + '%22}'; //?CallDate={"$lt":"05.06.16"} %22 means "
  callWithLinks.links.finterByCallDateGreaterThan = 'http://'+ req.headers.host + '/api/calls/?CallDate=' + '{%22$gt%22:%22' + callWithLinks.CallDate + '%22}';
  callWithLinks.links.finterByCallDateBetween = 'http://'+ req.headers.host + '/api/calls/?CallDate=' + '{%22$gt%22:%22' + callWithLinks.CallDate + '%22,%22$lt%22:%22' + callWithLinks.CallDate + '%22}';

  callWithLinks.links.finterByStartTime = 'http://'+ req.headers.host + '/api/calls/?StartTime=' + callWithLinks.StartTime;
  callWithLinks.links.finterByStartTimeLessThan = 'http://'+ req.headers.host + '/api/calls/?StartTime=' + '{%22$lt%22:%22' + callWithLinks.StartTime + '%22}'; //?StartTime={"$lt":"08:06:16"} %22 means "
  callWithLinks.links.finterByStartTimeGreaterThan = 'http://'+ req.headers.host + '/api/calls/?StartTime=' + '{%22$gt%22:%22' + callWithLinks.StartTime + '%22}';
  callWithLinks.links.finterByStartTimeBetween = 'http://'+ req.headers.host + '/api/calls/?StartTime=' + '{%22$gt%22:%22' + callWithLinks.StartTime + '%22,%22$lt%22:%22' + callWithLinks.StartTime + '%22}';

  callWithLinks.links.finterByStopTime = 'http://'+ req.headers.host + '/api/calls/?StopTime=' + callWithLinks.StopTime;
  callWithLinks.links.finterByStopTimeLessThan = 'http://'+ req.headers.host + '/api/calls/?StopTime=' + '{%22$lt%22:%22' + callWithLinks.StopTime + '%22}'; //?CallDate={"$lt":"05.06.16"} %22 means "
  callWithLinks.links.finterByStopTimeGreaterThan = 'http://'+ req.headers.host + '/api/calls/?StopTime=' + '{%22$gt%22:%22' + callWithLinks.StopTime + '%22}';
  callWithLinks.links.finterByStopTimeBetween = 'http://'+ req.headers.host + '/api/calls/?StopTime=' + '{%22$gt%22:%22' + callWithLinks.StopTime + '%22,%22$lt%22:%22' + callWithLinks.StopTime + '%22}';

  callWithLinks.links.finterByDiffTime = 'http://'+ req.headers.host + '/api/calls/?DiffTime=' + callWithLinks.DiffTime;
  callWithLinks.links.finterByDiffTimeLessThan = 'http://'+ req.headers.host + '/api/calls/?DiffTime=' + '{%22$lt%22:%22' + callWithLinks.DiffTime + '%22}'; //?DiffTime={"$lt":"12:00"} %22 means "
  callWithLinks.links.finterByDiffTimeGreaterThan = 'http://'+ req.headers.host + '/api/calls/?DiffTime=' + '{%22$gt%22:%22' + callWithLinks.DiffTime + '%22}';
  callWithLinks.links.finterByDiffTimeBetween = 'http://'+ req.headers.host + '/api/calls/?DiffTime=' + '{%22$gt%22:%22' + callWithLinks.DiffTime + '%22,%22$lt%22:%22' + callWithLinks.DiffTime + '%22}'; //?DiffTime={"$gt":"07:00", "$lt":"12:00"}
  //TODO other types of filters
  res.json(callWithLinks);
};

exports.editCall = function(req, res){
  req.body = _.pick(req.body, 'IP', 'CallType', 'CallDate', 'StartTime', 'StopTime', 'DiffTime');
  req.body.updatedOn = date().formatted;

  req.call = _.extend(req.call, req.body);
  
  req.call.save(function(err){
    if(err){
      res.status(500).send(err);
    }else{
      res.json(req.call);
    }
  });
};

exports.deleteCall = function(req, res){

  req.call.remove(function(err){
    if(err){
      res.status(500).send(err);
    }else{
      res.status(204).send();
    }
  });
};

///Controllers for Beaglebone 

exports.createcallsBBB = function(payload) {
  var req = {};
  req.body =  {
    IP: payload.IP,
    CallType: payload.CallType,
    CallDate: date().formattedDate,
    StartTime: date().formattedTime
  };
  //updating the payload
  exports.payload = _.extend(payload, req.body);
  
  Device.find({IP: payload.IP}, function (err, device) {
    if (err) {
      // tell front-end an error occured with payload.IP send(err);
    } else if (_.isEmpty(device)) {
      // tell front-end a IP is calling but not available in database send();
    } else {

      var call = new CallModel(req.body);

      call.save(function (err) {
        if (err) {
          // tell front-end an error occured while saving to database
        } else {

        }
      });
    }
  });
};


exports.nursePresence = function(payload) {

  CallModel.findOne().sort({createdAt:-1}).exec(function(err, data) {
    if (err) {
      // tell front-end an error occured
    } else if (_.isEmpty(data)) {
      // an ip is press the presence but it is not in calls table
    } else {
      data.StopTime = date().formattedTime;
      data.DiffTime = date().DiffTime(data.StartTime, data.StopTime);
      data.save();
    }
  });
}

//for Front-End
exports.fetchBBBinfo = function(payload) {
  //console.log(payload);
  Device.findOne({IP: payload.IP}, function (err, device) {
    if (err) {
      // tell front-end an error occured with payload.IP send(err);
      return {};

    } else if (_.isEmpty(device)) {
      // tell front-end a IP is calling but not available in database send();
      return {};
    } else {
      var newDevice = JSON.parse(JSON.stringify(device)); // cloning an object
      payload = _.extend(payload, newDevice);
      //console.log(payload);
      exports.payloadComplete = payload;
    }
  });
};