/**
 * Created by khaled on 6/5/2016.
 */

$(function() {
  var socket = io();
  /*payload = {
   IP:IP, //getting from network interfaces file IP='192.168.1.240'
   CallType: 'Normal'
   }*/

  socket.on('Normal Alert', function (payload) {
    var IP = payload.IP.split('.').join('_');
    var table = document.getElementById("callTableBody");
    var row = table.insertRow(0);
    row.className = "success";
    row.id = IP;
    console.log(payload);
    //BedNumber    RoomNumber   RoomType    Floor   CallType    CallDate    CallTime  Timer
    var newCell = row.insertCell(0);
    newCell.innerHTML = payload.BedNumber;
    newCell = row.insertCell(1);
    newCell.innerHTML = payload.RoomNumber;
    newCell = row.insertCell(2);
    newCell.innerHTML = payload.RoomType;
    newCell = row.insertCell(3);
    newCell.innerHTML = payload.Floor;
    newCell = row.insertCell(4);
    newCell.innerHTML = payload.CallType;
    newCell = row.insertCell(5);
    newCell.innerHTML = payload.CallDate;
    newCell = row.insertCell(6);
    newCell.innerHTML = payload.StartTime;
    newCell = row.insertCell(7);
    newCell.id = IP + "_timer";

    //start a timer
    $("#" + newCell.id).timer({
      duration: '23h59m30s',
      callback: function()
      {
        alert('Time up!');
      }
    });
  });

  socket.on('Presence Alert', function (payload) {
    var IP = payload.IP.split('.').join('_');
    $("#" + IP ).remove();
  });

  socket.on('Emergency Alert', function (payload) {
    var IP = payload.IP.split('.').join('_');
    var $txt1 = $('<div id="' + IP + '" class="alert alert-danger" role="alert">').text("Device IP: " + payload.IP + " CallType: " + payload.CallType);
    $("#container").append($txt1);
  });

  socket.on('BlueCode Alert', function (payload) {
    var IP = payload.IP.split('.').join('_');
    $("#" + IP).remove();
    var $txt1 = $('<div id="' + IP + '" class="alert alert-info" role="alert">').text("Device IP: " + payload.IP + " CallType: " + payload.CallType);
    $("#container").append($txt1);
  });

  socket.on('Cancel BlueCode Alert', function (payload) {
    var IP = payload.IP.split('.').join('_');
    $("#" + IP).remove();
  });


  
  
});